# Lab :  Résolution des Problèmes de Démarrage des Pods et Installation du Plugin Réseau Calico

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Objectifs

1. Résoudre le problème qui empêche les pods de démarrer

2. Vérifiez que vous pouvez communiquer entre les pods à l'aide du réseau de cluster

# Contexte

Vous travaillez pour une société appelée BeeBox, un service d'abonnement qui expédie des expéditions hebdomadaires d'abeilles aux clients. L'entreprise utilise Kubernetes pour exécuter son infrastructure d'applications conteneurisées.

La société a mis en place un nouveau cluster de développement qui sera utilisé par l'équipe de développement d'un sous-traitant externe. Le cluster semble fonctionner et l'équipe peut y accéder, mais elle signale qu'il y a un problème.

Vous avez reçu l'e-mail ci-dessous décrivant le problème. Résolvez le problème et vérifiez que les pods peuvent communiquer entre eux à l'aide du réseau Kubernetes.


```sh
Hello,
We've been trying to set up a network connection between two pods, called "cyberdyne-frontend" and "testclient". However, whenever we try to create these pods, they never get up and running. Can you look into this and figure out what is going on? Once the pods are up and running, we need you to verify they can communicate via network as well.

Thanks for your help!

Brando Smith
CyberDyne Systems
```

Remarque : Dans le laboratoire, au lieu de : kubectl apply -f https://docs.projectcalico.org/v3.14/manifests/calico.yaml , utilisez maintenant : kubectl apply -f https://docs.projectcalico.org/v3 .15/manifests/calico.yaml


>![Alt text](img/image.png)

# Application


## Étape 1 : Connexion au Serveur de Laboratoire

Connectez-vous au serveur de laboratoire à l'aide des informations d'identification fournies :

```sh
ssh -i id_rsa user@PUBLIC_IP_ADDRESS
```

## Étape 2 : Vérification de l'État des Pods et des Nœuds

1. Répertoriez les pods pour vérifier leur état :

```sh
kubectl get pods
```

>![Alt text](img/image-1.png)
*Liste de pods en corus*


2. Vérifiez l'état des nœuds :

```sh
kubectl get nodes
   ```

>![Alt text](img/image-2.png)
*Liste de nœuds*

   - Il semble que les nœuds soient `NotReady`.

3. Décrivez un nœud pour obtenir plus d'informations :

```sh
kubectl describe node k8s-worker1
```

>![Alt text](img/image-3.png)
*Description du worker1*

   - Il semble que le composant `kube-proxy` soit bloqué au démarrage.

4. Vérifiez l'état des pods du plug-in réseau :

```sh
kubectl get pods -n kube-system
```
>![Alt text](img/image-4.png)
*Liste des pods system*

   - Les pods réseau est down. Très probablement, un plugin réseau n'a pas été installé.

## Étape 3 : Installation du Plugin Réseau Calico

1. Installez le plugin réseau Calico :

```sh
kubectl apply -f https://docs.projectcalico.org/v3.15/manifests/calico.yaml
```

>![Alt text](img/image-5.png)
*Installation du plugin réseau réussi*

2. Vérifiez à nouveau l'état des nœuds et des pods :

```sh
kubectl get nodes
```

>![Alt text](img/image-7.png)

   - Les nœuds devraient être `Ready` après environ une minute.

```sh
kubectl get pods
```

>![Alt text](img/image-6.png)
*Pods en cours d'exécutation*

>![Alt text](img/image-8.png)
*Serveur DNS disponible *

## Étape 4 : Vérification de la Communication entre les Pods

1. Vérifiez que les pods peuvent communiquer sur le réseau :

```sh
kubectl get pods -o wide
```

>![Alt text](img/image-9.png)
*Liste de pods et ip*


2. Exécutez `curl` sur l'adresse IP du pod `cyberdyne-frontend` (qui sera répertoriée dans le résultat de la commande précédente) depuis le pod `testclient` :

```sh
kubectl exec testclient -- curl <cyberdyne-frontend_POD_IP>
```

dans notre cas : 

```sh
kubectl exec testclient -- curl 192.168.194.69
```

>![Alt text](img/image-10.png)
*Reponse réseau du pod*

   - Le résultat doit être le HTML d'une page Nginx, ce qui signifie que les pods sont capables de communiquer.

Ce lab couvre la résolution des problèmes de démarrage des pods en vérifiant l'état des nœuds et des pods, en installant le plugin réseau Calico, et en vérifiant que les pods peuvent communiquer entre eux via le réseau de cluster.